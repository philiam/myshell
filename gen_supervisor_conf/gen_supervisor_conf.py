#!/usr/bin/env python
# coding:utf-8
# create time: 15-2-3 上午10:59
__author__ = 'zhangtao'

import os
import sys


def main():
    """
    自动生成 supervisor 的 program 配置文件 .conf
    生成到当前目录 和 /etc/supervisord.conf.d/ 目录下（如果supervisord存在的话）
    example:
       gsuper tracker.py
       当前目录下会生成 tracker.conf文件
       和 /etc/supervisord.conf.d/
    """
    # cmd = "find . -name $1|xargs grep $2"
    # print(sys.argv.__len__())
    # print("脚本名：%s" % sys.argv[0])
    # for i in range(1, sys.argv.__len__()):
    #     print("参数%s:%s" % (i, sys.argv[i]))

    if sys.argv.__len__() == 1:
        print(main.__doc__)
        return
    elif sys.argv.__len__() == 2:
        cmd = "grep -R '%s' ./" % sys.argv[1]
    else:
        cmd = "find ."
        for i in range(2, sys.argv.__len__()):
            cmd += " -name '%s' -o " % sys.argv[i]
        cmd = cmd.strip(" -o ")
        print(cmd)
        cmd += "|xargs grep '%s'" % sys.argv[1]
    print(cmd)
    os.system(cmd)


if __name__ == "__main__":
    main()