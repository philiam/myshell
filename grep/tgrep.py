#!/usr/bin/env python
# coding:utf-8
# create time: 14-12-26 上午11:55
__author__ = 'zhangtao'

import os
import sys


def main():
    """
    在当前及子目录下查找指定文件中的指定字符串:
        tgrep "要查的字符串" "*.py"
    可以指定多个文件类型：
        tgrep "字符串" "*.py" "*.sql" "*.conf"
    文件类型中带有通配符*时要加引号

    缺省为所有文件类型
    """
    # cmd = "find . -name $1|xargs grep $2"
    # print(sys.argv.__len__())
    # print("脚本名：%s" % sys.argv[0])
    # for i in range(1, sys.argv.__len__()):
    #     print("参数%s:%s" % (i, sys.argv[i]))

    if sys.argv.__len__() == 1:
        print(main.__doc__)
        return
    elif sys.argv.__len__() == 2:
        cmd = "grep -R '%s' ./" % sys.argv[1]
    else:
        cmd = "find ."
        for i in range(2, sys.argv.__len__()):
            cmd += " -name '%s' -o " % sys.argv[i]
        cmd = cmd.strip(" -o ")
        print(cmd)
        cmd += "|xargs grep '%s'" % sys.argv[1]
    print(cmd)
    os.system(cmd)


if __name__ == "__main__":
    main()