# coding: utf-8
# author: ZhangTao
# Date  : 2016/5/26
from __future__ import unicode_literals
import time
import pyperclip
if __name__ == '__main__':
    t = time.time()
    text = str(long(t))
    print(text)
    pyperclip.copy(text)
    # p = pyperclip.paste()
    print('It has been copied to clipboard !')
