#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2016/5/12
from __future__ import unicode_literals
import os
import commands
import re
import colorsys


def print_hard_info():
    cpu_cmd = 'grep "model name" /proc/cpuinfo | cut -f2 -d:'
    os_cmd = 'cat /etc/redhat-release'
    ram_cmd = 'free -h'
    status, output = commands.getstatusoutput(os_cmd)
    print('OS: %s' % output)

    status, output = commands.getstatusoutput(cpu_cmd)
    cpus = output.split('\n')
    print('CPU: %s %s核心' % (cpus[0].strip(), len(cpus)))

    # get physical memory in kb
    meminfo = open('/proc/meminfo').read()
    # 匹配格式 MemTotal:       131619760 kB
    matched = re.search(r'^MemTotal:\s+(\d+)', meminfo)
    mkb = int(matched.groups()[0])
    mGB = mkb/1024/1024
    print('RAM: %s G' % mGB)

if __name__ == '__main__':
    print_hard_info()







