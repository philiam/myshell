#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2016/8/22
# from __future__ import unicode_literals
# from os import system as s
from os.path import exists as e
import os
import uuid
import sys
import subprocess
"""
linux 系统监控
glances + influxdb + Grafana
收集->存储->展示
"""


def s(cmd):
    print cmd
    os.system(cmd)


def ss(shell):
    tmpf = 'tmp%s' % uuid.uuid1()
    sh = '\n'.join([cmd.strip() for cmd in shell.split('\n')])
    # print sh
    f = open(tmpf)
    f.write(sh)
    f.close()
    s('/bin/bash ' + tmpf)
    os.remove(tmpf)


def m():
    all = False
    if len(sys.argv) > 1:
        if sys.argv[1] == '-a':
            all = True
    # if not e('/etc/supervisord.conf'):
    #     s('install_env')
    s('pip install glances')
    s('pip install influxdb')
    s('mkdir /etc/glances/')
    s('cp ../gig/glances.conf /etc/glances/')
    # s('cp ../gig/glances_influxdb.super.conf /etc/supervisord.conf.d/')
    s('cp ../gig/glances-influxdb.service /etc/systemd/system/')
    s('echo 安装 influxdb')
    s('cp ../gig/influxdb.repo /etc/yum.repos.d/')
    s('yum -y install influxdb')
    s('systemctl start influxdb')
    s('curl -i -XPOST http://localhost:8086/query --data-urlencode "q=CREATE DATABASE glances"')
    s('systemctl start glances-influxdb')
    if all:
        s('echo 安装 Grafana')
        s('yum -y install https://grafanarel.s3.amazonaws.com/builds/grafana-3.1.1-1470047149.x86_64.rpm')
        s('systemctl start grafana-server')


if __name__ == '__main__':
    m()

