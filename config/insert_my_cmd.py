#!/usr/bin/env python
# coding:utf-8
# create time: 15-1-28 下午3:08
__author__ = 'zhangtao'
import os

basepath = os.path.dirname(os.path.dirname(__file__))
cmds = [
    ("grep/tgrep.py", "tgrep"),
]

def make_link(cmds):
    for t in cmds:
        if isinstance(t, tuple):
            fp = os.path.join(basepath, t[0])
            if len(t) == 1:
                shell = "ln -s %s /usr/local/bin/" % fp
                print(shell)
                os.system(shell)
            else:
                shell = "ln -s %s /usr/local/bin/%s" % (fp, t[1])
                print(shell)
                os.system(shell)

if __name__ == "__main__":
    make_link(cmds)



