#!/usr/bin/env python
# coding:utf-8
# create time: 15-1-28 下午4:37
__author__ = 'zhangtao'
import os
# 将 myshell的bin 目录添加的系统的环境变量 PATH


basepath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def searchstr(filepath, s):
    """
    查找文件重工指定的字符串
    :param filepath:
    :param s:
    :return:
    """
    with open(filepath) as f:
        for line in f:
            if line.__contains__(s):
                # print(line)
                return True
    return False


def main():
    bashrc = os.path.expanduser("~/.bashrc")
    myshell_path = os.path.join(basepath, "bin")
    append_content = "PATH=$PATH:%s" % myshell_path
    # 检测 此环境变量是否已经添加
    if not searchstr(bashrc, append_content):
        ap = open(bashrc, 'a')
        ap.write("\n%s" % append_content)
        ap.close()
        os.system("source %s" % bashrc)
        print("add succeed !")
    else:
        print("myshell also exist !")

if __name__ == "__main__":
    main()
