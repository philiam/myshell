#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2016/7/29
# from __future__ import unicode_literals
# 安装 centos 7 系统中常用 的组件
# from os import system as s
from os.path import exists as e
import os
import uuid
import subprocess


def s(cmd):
    print cmd
    os.system(cmd)


def ss(shell):
    tmpf = 'tmp%s' % uuid.uuid1()
    sh = '\n'.join([cmd.strip() for cmd in shell.split('\n')])
    # print sh
    f = open(tmpf, 'w')
    f.write(sh)
    f.close()
    s('/bin/bash ' + tmpf)
    os.remove(tmpf)


def m():
    home = os.path.expanduser('~')
    s('yum -y install vim git wget python-devel gcc gcc-c++')
    if e('%s/.vim_runtime' % home):
        print('* The Ultimate vimrc has already been installed!')
    else:
        s('git clone https://git.oschina.net/philiam/vimrc.git ~/.vim_runtime')
        s('sh ~/.vim_runtime/install_awesome_vimrc.sh')
        # 将 vi 彻底替换掉
        s('/usr/bin/mv /usr/bin/vi /usr/bin/vi.bak')
        s('/usr/bin/cp /usr/bin/vim /usr/bin/vi')

    # 不在使用supervisord 管理后台服务，使用centos7 的 systemd
    # if e('/etc/supervisord.conf'):
    #     print('* The supervisord has already been installed!')
    # else:
    #     ss("""
    #         easy_install pip
    #         # 配置supervisord
    #         pip install supervisor
    #         echo_supervisord_conf > /etc/supervisord.conf
    #         mkdir /etc/supervisord.conf.d/
    #         mkdir /var/log/supervisord/
    #         echo [include]>>/etc/supervisord.conf
    #         echo 'files = /etc/supervisord.conf.d/*.conf'>>/etc/supervisord.conf
    #         supervisord
    #     """)


if __name__ == '__main__':
    m()