#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2017/10/12
# from __future__ import unicode_literals
import paho.mqtt.publish as publish
import json
import time
import requests
# 通过influxdb 恢复拼图数据

fidis_key ={
    'forbon': ['L2_1_132_2_101_0', 'L2_1_132_2_101_1', 'K2_1_132_2_101_0', 'K2_1_132_2_101_1'],
    'xinlei': ['L1_3_7_0', 'L1_3_7_1']
}


def get_last(client, fidis):
    print(client)
    url = "http://c2.gards.top:8086/query"

    keys = fidis_key[fidis]

    s = ''
    for key in keys:
        s += "last({0}) as {0},".format(key)

    s = s[:-1]
    q = 'select {} from "{}" where time > 1514789054s'.format(s, client)
    data = {
        'db': fidis,
        'q': q,
    }
    # print(q)
    rs = requests.get(url, params=data)
    # print(rs.text)
    try:
        value = rs.json()['results'][0]['series'][0]['values'][0]
        pjson = dict()

        for i in range(len(keys)):
            if not value[i+1] is None:
                pjson[keys[i]] = value[i+1]
                # print("{}=None".format(keys[i]))
        print(pjson)
        if pjson:
            publish.single("r", json.dumps(pjson),
                           client_id=client,
                           hostname="g1.gards.top",
                           auth={'username': fidis, 'password': fidis + '123'})
    except KeyError as e:
        print(e)
    # for key in keys:
    #     if


def get_all_client(fidis):
    url = "http://c2.gards.top:8086/query?db={}&q=show series where topic='r'".format(fidis)
    rs = requests.get(url)
    clients = []
    if rs.text:
        values = rs.json()['results'][0]['series'][0]['values']
        for t in values:
            clients.append(t[0].split(',')[0])
            # print(t[0].split(',')[0])
    # print(rs.text)

    return clients

# get_all_client('forbon')
# get_last('865067022403045', 'forbon')


def recovery(fidis):
    clients = get_all_client(fidis)
    # clients = ['A22016122100931', 'A22016122100145']
    print("total {}".format(len(clients)))
    for client in clients:
        time.sleep(0.1)
        try:
            get_last(client=client, fidis=fidis)
        except:
            print('except')

# recovery('xinlei')
recovery('forbon')
