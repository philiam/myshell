#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2016/9/26
# from __future__ import unicode_literals
# Echo client program
import socket
import sys


# -*- coding: utf-8 -*-
import socket

# apway 连接测试

def check_tcp_status(ip, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (ip, port)
    print 'Connecting to %s:%s.' % server_address
    import struct
    flag = '?'
    cmd = 0
    pay = "A22016122100503,211.154.146.60:1883"
    length = len(pay) + 1
    msg = struct.pack("!1s2B%ss" % len(pay), flag, length, cmd, pay)
    # m1 = struct.pack("!2B", length, cmd)
    # m2 = struct.pack("%ss" % len(pay), pay)
    print("s %r" % msg)
    sock.connect(server_address)
    sock.send(msg)
    # sock.send(m1)
    # sock.send(m2)
    data = sock.recv(512)
    print('r %r' % data)

    sock.send("uuuuuu")
    # sock.close()
    data = sock.recv(512)
    print('r %r' % data)
    sock.close()

if __name__ == "__main__":
    check_tcp_status("apway.gards.top", 8000)
