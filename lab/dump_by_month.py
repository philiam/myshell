#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2017/10/20
from __future__ import unicode_literals
import time
import requests
import json


def dump(imei, fidis):

    url = "http://c2.gards.top:8086/query"
    f = open(imei + '.txt', mode='w')
    dt1 = "2017-08-01 00:00:00"
    dt2 = "2017-09-01 00:00:00"
    timeArray = time.strptime(dt1, "%Y-%m-%d %H:%M:%S")
    ts1 = int(time.mktime(timeArray))
    timeArray2 = time.strptime(dt2, "%Y-%m-%d %H:%M:%S")

    ts2 = int(time.mktime(timeArray2))
    start = ts1
    end = 0
    while end < ts2:
        # 每次查询3个小时
        end = start + 3 * 60 * 60
        if end > ts2:
            end = ts2
        q = 'select payload from "{}" where time > {}s and time < {}s'.format(imei, start, end)
        print(q)
        params = {
            "pretty": "true",
            "db": fidis,
            "q": q
        }

        rs = requests.get(url, params=params)
        # print(rs.text)
        if 'series' in rs.json()['results'][0]:
            values = rs.json()['results'][0]['series'][0]['values']
            for row in values:
                f.write(json.dumps(row) + '\n')
        time.sleep(0.08)
        start = end


dump('865067021743433', 'xinlei')
