#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2016/9/5
from __future__ import unicode_literals
import sys
from multiprocessing import Process, Manager
import os


def main():
    if sys.argv.__len__() < 2:
        print(sys.argv)
        return
    fp = sys.argv[1]
    fn = os.path.basename(fp)
    fp = os.path.abspath(fp)
    fpw = fp + ".txt"
    f2 = open(fpw, 'w')
    f = open(fp)
    for line in f:
        if line.strip():
            f2.write(line)
    f.close()
    f2.close()
    print ("完毕")

main()
