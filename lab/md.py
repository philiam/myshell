#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2016/10/28
from __future__ import unicode_literals
import markdown2
text="""
## 修改python 库安装源

在~/.pip/pip.conf（默认没有此文件） 中添加

```ini
[global]
index-url = http://mirrors.aliyun.com/pypi/simple/
[install]
trusted-host=mirrors.aliyun.com
```

如果只安装单个python库时指定源，命令如下：
```
pip install django --trusted-host mirrors.aliyun.com
```
*因为镜像服务器不是`https`所以需要 加`--trusted-host`*
"""
html = markdown2.markdown(text)