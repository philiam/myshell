#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2017/6/1
from __future__ import unicode_literals
import requests
# import datetime
import time
import os
import json


def dump():
    url = "http://c2.gards.top:8086/query"
    imei = 'A22016122100575'
    dt = "2017-05-18 00:00:00"
    # timeArray = time.strptime(dt, "%Y-%m-%d %H:%M:%S")
    total = 30000
    limit = 10000
    offset = 0
    f = open(imei + '.txt', mode='w')
    while True:

        if total <= 0:
            break

        if total > 10000:
            limit = 10000
            total -= 10000
            print total
        else:
            limit = total
            total = 0

        q = "select payload from {} order by time desc limit {} offset {}".format(imei, limit, offset)
        params = {
            "pretty": "true",
            "db": "kingpower",
            "q": q
        }
        rs = requests.get(url, params=params)

        # print rs.text
        if 'series' in rs.json()['results'][0]:
            values = rs.json()['results'][0]['series'][0]['values']
            for row in values:
                f.write(json.dumps(row) + '\n')
        print limit
        time.sleep(1)
        if limit < 10000:
            break
        offset += limit

dump()