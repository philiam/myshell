#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2017/6/1
from __future__ import unicode_literals
import requests
# import datetime
import time
import os


def dump():
    url = "http://c2.gards.top:8086/query"
    imei = 'A22016122100993'
    dt = "2017-05-18 00:00:00"
    timeArray = time.strptime(dt, "%Y-%m-%d %H:%M:%S")
    #转换成时间戳
    timestamp = time.mktime(timeArray)
    overtime = time.time()
    os.mkdir(imei)
    while timestamp < overtime:
        time_local = time.localtime(timestamp)
        fn = time.strftime("{}/%Y-%m-%d.txt".format(imei), time_local)

        ts = int(timestamp*1E9)

        timestamp_end = timestamp + 24*60*60

        ts2 = int(timestamp_end*1E9)

        q = "select payload from {} where time >= {} and time <{}".format(imei, ts, ts2)
        timestamp = timestamp_end
        params = {
            "pretty": "true",
            "db": "fangkuai",
            "q": q
        }
        rs = requests.get(url, params=params)
        # print rs.text
        if 'series' in rs.json()['results'][0]:
            f = open(fn, mode='w')
            f.write(rs.text)
            f.close()

        # if 'series' in rs.json()['results'][0]:
        #     values = rs.json()['results'][0]['series'][0]['values']
        #     f = open(fn, mode='w')
        #     for row in values:
        #         f.write("{},{}".format(row[0], row[1]))
        #     f.close()

dump()