#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2017/5/16
from __future__ import unicode_literals
import requests
import json


STA = {'K1': 3500.0, 'K2': 900.0, 'K3': 1350.0, 'K4': 2000.0, 'K5': 2300.0, 'K6': 0.9, 'K7': 4800.0, 'K8': 1700.0}
EVENT = {'K9': 0, 'K10': 0}
FLT = {'K11': 0}
ALT = {'K12': 0}


def get_change(grid):
    has_sta = 0
    has_event = 0
    has_fault = 0
    has_alert = 0

    for k in grid:
        if k in STA and not has_sta:
            if abs(grid[k]-STA[k])/STA[k] >= 0.4:
                has_sta = 1
        elif k in EVENT and not has_event:
            if grid[k] != EVENT[k]:
                has_event = 2
        elif k in FLT and not has_fault:
            if grid[k] != FLT[k]:
                has_event = 4
        elif k in ALT and not has_alert:
            if grid[k] != ALT[k]:
                has_event = 8

    return has_sta + has_alert + has_event + has_fault


def dump(limit=10000):

    url = "http://c2.gards.top:8086/query"
    imei = 'DE2017042400000'
    # select count(v) from index where imei='DE2017042400000' and time > 1493803581374283378 - 1h
    ts = 1494921600000000000
    params = {
        "pretty": "true",
        "db": "demo",
        "q": "select payload from data where imei='{}' and time > {} - 1m limit {}".format(imei, ts, limit)
    }

    rs = requests.get(url, params=params)
    # print rs.text
    # f = open(imei+'.txt', mode='w')
    # print (rs.text)
    jr1 = rs.json()
    # f.write(rs.text)

    params['q'] = "select v from index where imei='{}' and time > {} - 1m limit {}".format(imei, ts, limit)

    rs = requests.get(url, params=params)
    jr2 = rs.json()
    # print (rs.text)
    v2 = jr2['results'][0]['series'][0]['values']
    v1 = jr1['results'][0]['series'][0]['values']

    f = open(imei+'.txt', mode='w')
    for i in range(len(v1)):
        v2[i][1] = int(v2[i][1])
        if i == 0:
            v2[i].append(0)
            v2[i].append(0)
            v2[i].append(0)
        else:
            # 当前与上一个指数差
            # v2[i][2]
            v2[i].append(v2[i][1]-v2[i-1][1])

            # v2[i][3]
            v2[i].append(v2[i-1][2])

            # v2[i][4]
            v2[i].append(v2[i-1][3])

        # payload
        grid = json.loads(v1[i][1])
        # sp = json.dumps(jp, sort_keys=True)
        gc = get_change(grid)
        v2[i].append(gc)
        f.write(json.dumps(v2[i])+'\n')

    # f.write(json.dumps(v2, indent=4))
    f.close()


def dumpx(limit=2000):
    url = "http://c2.gards.top:8086/query"
    imei = 'DE2017042400000'
    # select count(v) from index where imei='DE2017042400000' and time > 1493803581374283378 - 1h
    ts = 1494921600000000000
    tz = "2017-05-20T09:07:19.342297909Z"
    params = {
        "pretty": "true",
        "db": "demo",
        "q": "select i,d1,d2,d3,x from index where imei='{}' limit {}".format(imei, limit)
    }
    keys = ["K{}".format(i) for i in range(1, 13)]
    keys = ','.join(keys)
    params2 = {
        "pretty": "true",
        "db": "demo",
        "q": "select {} from data where imei='{}' and time > '{}' limit {}".format(keys, imei, tz, limit)
    }

    f = open(imei + '.txt', mode='w')
    rs = requests.get(url, params=params)
    values = rs.json()['results'][0]['series'][0]['values']

    rs2 = requests.get(url, params=params2)
    values2 = rs2.json()['results'][0]['series'][0]['values']

    for i, row in enumerate(values):
        del values2[i][0]
        for j in range(1, 6):
            row[j] = round(row[j], 2)
        row.extend(values2[i])
        f.write(json.dumps(row)+',\n')

    # 第二次查询
    last_time = values[-1][0]
    params['q'] = "select i,d1,d2,d3,x from index where imei='{}' and time > '{}' limit {}".format(imei, last_time, limit)
    rs = requests.get(url, params=params)
    values = rs.json()['results'][0]['series'][0]['values']

    params2['q'] = "select {} from data where imei='{}' and time > '{}' limit {}".format(keys, imei, last_time, limit)
    rs2 = requests.get(url, params=params2)
    values2 = rs2.json()['results'][0]['series'][0]['values']

    for i, row   in enumerate(values):
        del values2[i][0]
        for j in range(1, 6):
            row[j] = round(row[j], 2)
        row.extend(values2[i])
        f.write(json.dumps(row)+',\n')
    # print rs.text


dumpx(limit=10000)

