# coding: utf-8
# author: ZhangTao
# Date  : 2016/8/10
from __future__ import unicode_literals
import requests
import json


def get_invt():
    url = 'http://127.0.0.1:6984/gardsdb/_design/tongji/_view/created_client?startkey=["2016-09-01"]&endkey=["2016-09-02"]&group_level=2'
    r = requests.get(url=url)
    rs = r.json()['rows']
    print len(rs)
    for e in rs:
        print("%s\t%s" % (e['key'][1], e['value']))


# get_invt()
def t_sum(n):
    s = 0
    for i in range(1, n+1):
        s += i
    print s

# t_sum(10000000)

import math
def g(n):
    i=3
    for i in range(3, n+1):
        for j in range(i, n+1):
            c = math.sqrt(i*i + j*j)
            if c-int(c) == 0 and c < n+1:
                print ("%s,%s,%s" % (i, j, int(c)))


def get_invt_list():
    url = "http://config.invt.com:9600/api"
    data = {
        "t": "GET",
        "u": "gards_terminal?category_id=GARDS&group_id=GARDS_TEST&limit=10&offset=1",
    }
    r = requests.post(url=url, data=data)
    print r.text


def test_history():
    url = "http://211.154.146.58:8888/dbapi/client_created?client=A22016122100948&create1=2017-07-20%2000:00:00&create2=2017-07-21%2011:00:00&limit=50&message_type=r"
    r = requests.get(url=url, auth=('mixlinker', 'gards123456'))
    print(r.text)


# test_history()

def DataStatics():
    import requests
    t = 1501516800
    s = 0
    u = "http://c2.gards.top:8086/query?db=xinlei&q=select payload from A22016122100902 where time > {}s and time < {}s"
    for i in range(24*31):
        t2 = t + 60 * 60
        url = u.format(t, t2)
        t = t2
        rs = requests.get(url)
        print (i)
        # print rs.text
        # day = rs.json()['results'][0]['series'][0]['values']
        # print len(day)
        s += len(rs.text)
    print s

DataStatics()
